/*
 * 
 * Author: Joao Pereira  (up201909554@fe.up.pt)
 * 		   Roberto Lopes (up201606445@fe.up.pt)
 * 
 * University: FEUP MIEEC SELE
 * 
 * Date: 17/12/2020
 * 
 * 
 */

#include <Arduino.h>
#include "twi.h"
#include "register_set.h"
#include "commands.h"

#define FOSC 16000000 // Clock Speed #define BAUD 9600
#define BAUD 9600
#define UBRR FOSC/16/BAUD-1

#define RED 3
#define GREEN 6
#define BLUE 5

int integration_delay = 700; //700ms
int gain = 1;

const uint8_t gammatable[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

uint16_t clear_color, blue_color, green_color, red_color;

void USART_Init(unsigned int ubrr) {
    /*Set baud rate */
    UBRR0H = (unsigned char)(ubrr>>8); 
    UBRR0L = (unsigned char) ubrr;
    /* Enable receiver and transmitter */ 
    UCSR0B = (1<<RXEN0)|(1<<TXEN0)/*|(1<<UCSZ02)*/;
    /* Set frame format: 9 data, 1 stop bit. odd parity bit*/ 
    //UCSR0C = (1<<UCSZ00)|(1<<UCSZ01)|(1<<UPM01)|(1<<UPM00);
}


void setup() {

	//Outputs Setup
  	DDRD |=  (1<<PD3); //RED LED
	DDRD |=  (1<<PD5); //BLUE LED
	DDRD |=  (1<<PD6); //GREEN LED

  	//Serial.begin(9600);
	USART_Init(UBRR);

	char string[10];
	sprintf(string, "%d", TWISetup()) ;
	UART_pushString("|	  SCL Frequency -> "); UART_pushString(string); UART_pushString(" kHz	  |\n");
	
	/* Check if the device is connected */
  	while (getID() != NUMBER_IDENTIFICATION) {
  		UART_pushString("Device is not connected!\n");
		/* Force boot */
		powerON();
		_delay_ms(1000);
  	} 

	
    UART_pushString("|	  Device is connected!		  |\n");
	
	setGain();
	sprintf(string, "%d", gain) ;
	UART_pushString("|	  GAIN: "); UART_pushString(string); UART_pushString("			  |\n");

	setTime();
	sprintf(string, "%d", integration_delay) ;
	UART_pushString("|	  Integration Delay: "); UART_pushString(string); UART_pushString("ms	  |\n");

	enableTCS34725();
	//UART_pushString("c  r  g  b \n");
	UART_pushString("r  g  b \n");
}

void loop() {

	getRawColors(&clear_color, &red_color, &green_color, &blue_color);

	uint32_t sum = clear_color;
  	float r, g, b;
  	r = red_color; 
	r /= sum;
  	g = green_color; 
	g /= sum;
  	b = blue_color;
	b /= sum;
  	r *= 256; g *= 256; b *= 256;

	analogWrite(RED, gammatable[(int)r]);
	analogWrite(GREEN, gammatable[(int)g]);
	analogWrite(BLUE, gammatable[(int)b]);

	char string1[10];
	char string2[10];
	char string3[10];
	char string4[10];
	#ifdef DEBUG
		
		//sprintf(string1, "%d", gammatable[(int)clear_color]) ;
		//UART_pushString(string1); 
		//UART_pushString(" ");
		sprintf(string2, "%d", gammatable[(int)r]) ;
		UART_pushString(string2); 
		UART_pushString(" ");
		sprintf(string3, "%d", gammatable[(int)g]) ;
		UART_pushString(string3);
		UART_pushString(" ");
		sprintf(string4, "%d", gammatable[(int)b]) ;
		UART_pushString(string4); 
		UART_pushString("\n");
	#endif

}
# TASK2 - Sensor Lux RGB TCS34725 (I2C/TWI)

The aim of this project is to implement TWI protocol to communicate between an Arduino Nano and a TCS34725 RGB sensor. 
###### [Datasheet]( https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf)

## Contents

## System Concept
![Imagem1](https://user-images.githubusercontent.com/61596101/102629207-bd080b00-4142-11eb-8117-46ff7f6fb525.png)

## I2C & Two Wire Interface(TWI)

### Explanation

I2C is a serial communication protocol used to communicate between masters and slaves using a two-wire interface. Two Wire Interface was created by Atmel to avoid any legal troubles using this communication protocol created by Philips. In this project it will be use a microcontroller from Atmel (atmega328 from an Arduino Nano), implemented using TWI.

### Two Wire Interface 

The two wire interface is defined by two wires: **SDA** (serial data) and **SCL** (serial clock) connected with a pull-up resistor to +Vdd. This allows the communication between many devices, with their unique address, with only two wires.

In a normal state both lines are high.
To start a communication, a master creates a **start condition** (SDA line low) and to end communication the master sends a **stop condition** (SDA line high). Between these conditions the data is sent.

After sending a **start condition** the master must send the **7-bit slave address** of the device that he wants to establish connection followed by **read/write bit** (where read is 1 and write is 0). 
If the slave is on the bus, it answers with an ACK. If not, with NACK.

If the slave answers with ACK, the master should now become a master transmitter or a master receiver, depending on the **read/write bit**. In this project both modes are used.

To transmit, a master must send the command, the desired data packets and then send a stop condition to end communication:

![Imagem2](https://user-images.githubusercontent.com/61596101/102629209-bda0a180-4142-11eb-9207-8c3ee1b38820.png)

The master after receiving the data send either a ACK or NACK, Sends the NACK when it is the last packed received before a stop condition. 

In this example, in the [Datasheet of the sensor]( https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf) it is also notable that to change from master transmitter to master receiver, the master must send a *repeated start condition and send the proper **slave address + R/W bit**: 

![Imagem3](https://user-images.githubusercontent.com/61596101/102629210-be393800-4142-11eb-9517-0b76629a9bfa.png)


## Implementation

A lib folder was created with the I2C/TWI protocol implemented in C.

#### Setup

To setup TWI it is necessary to set the SCL frequency. Since the maximum rate is 400kHz and the TCS34725 works up to 400kHz, the SCL frequency is set a bit lower than the maximum values, around 333kHz:

Arduino Frequency: 16MHz | Prescaler: 16 | TWEN set to 1 to enable TWI operation

#### Start Condition

A start condition is created by setting TWSTA and TWINT to 1. TWSTA is written to 1 to check if the bus is free and generates a start condition on the bus if it is free. TWINT is set by hardware when the TWI has finished its current task. Wait for TWINT to be set.

#### Write

To write to the bus the TWI Data Register (TWDR) should be written with the data desired to send. Enable TWI operation and wait for it to end.

#### Read

To read, the master waits reception, reads on TWDR and sends ACK to the slave by enable the TWI Enable Acknowledge Bit (TWEA). In case of reception of the last packet before the stop condition, a NACK must be sent by set TWEA to 0.

#### Stop Condition

A stop condition is created by setting TWI STOP Condition Bit (TWSTO) to 1.

## TCS34725 

#### States

Looking at the detailed state diagram of this Sensor it is divided into 4 states. The Sensor is on a Sleep state until it receives a TWI operation start and changes to Idle state (PON register set to 1). It is not capable of reading colors before changing to RGBC state. To start reading RGB values, the sensor must enter the RGBC state by enable AEN.

![Imagem4](https://user-images.githubusercontent.com/61596101/102629211-be393800-4142-11eb-9789-415a8aa28081.png)

Between the Idle state and the RGBC state the device needs a 2.4ms delay as stated on datasheet. And between each reading, the RGBC takes 2.4ms to 700ms.

#### Register Set

The device can be controlled by the following instructions:

![Imagem5](https://user-images.githubusercontent.com/61596101/102629212-be393800-4142-11eb-9050-2d398e429b78.png)

#### ID
So to get, for example, the device ID, it is necessary to send a Start Condition; the Slave Address and the W bit, informing the slave the incoming data is an instruction; the instruction: COMMAND (0x80) + ID (0x12); send a repeated start condition in order to change to master receiver; send again the Slave Address, but now with the R bit; receive the ID & finally send a stop condition.
The reception of the device ID guarantees that the slave is connected to master and the connection is well established.

#### RGB

The clear, red, green, and blue data is stored as 16-bit values. So, the master should *read* twice to get a proper color value. Is necessary to execute the following steps: send a Start Condition; the Slave Address and the W bit, informing the slave the incoming data is an instruction; the instruction: COMMAND + COLOR (CDATA, RDATA, GDATA or BDATA); send a repeated start condition to change to master receiver; send again the Slave Address, but now with the R bit; Receive the first 8 bits; Receive the last 8 bits & finally send a stop condition.

#### Integration Time and Gain

The ADCs of the sensor is controlled by two variables: integration time and gain. The integration time controls the RGBC Timing Register and the number of the cycles. The gain controls the sensor light sensitivity. To set these two, the process is similar. For example, to set gain: Send a Start Condition; the Slave Address and the W bit, informing the slave the incoming data is an instruction; Send the instruction: COMMAND + CONTROL_REGISTER (0x0F); Send the gain value: AGAIN (00x0 for 1x Gain) & finally send a stop condition.

### Design & PCB

#### Schematic

![Imagem6](https://user-images.githubusercontent.com/61596101/102629213-bed1ce80-4142-11eb-9a38-024881524264.png)

#### PCB Design

![Imagem7](https://user-images.githubusercontent.com/61596101/102629216-bed1ce80-4142-11eb-9824-8b366b9fed69.png)

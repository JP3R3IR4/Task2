/*
 * 
 * Author: Joao Pereira  (up201909554@fe.up.pt)
 * 		   Roberto Lopes (up201606445@fe.up.pt)
 * 
 * University: FEUP MIEEC SELE
 * 
 * Date: 17/12/2020
 * 
 * 
 */

#include "twi.h"
#include <Arduino.h>

void UART_pushChar(char c) {
    // Wait until the register to write to is free.
    loop_until_bit_is_set(UCSR0A, UDRE0);

    // Write the byte to the register.
    UDR0 = c;
}
void UART_pushString(char *data) {
    // Loop until end of string writing char by char.
    while(*data){
      UART_pushChar(*data++);
    }
}

int TWISetup(){

    int sclf;
    // Fscl = 333kHz  (max400kHz)  Fcpu = 16MHz //
    // Set I2C Bit Rate Register (16 bits) //
    TWBR = 0b00010000;
    TWCR = (1<<TWEN);

    sclf = (int) CPU_F / (int) (16 + (2*TWBR));
    return sclf;

}

uint8_t TWIStart() {

    TWCR = 0;

    /* Enable and start TWI */
    TWCR = (1<<TWEN)|(1<<TWSTA)|(1<<TWINT);
    while (!(TWCR & (1<<TWINT)));

    /* Check if Status code Errors */
    if ((TWSR == BUS_ERROR) ||
        (TWSR == ARBILOST)) {

            #ifdef DEBUG 
                UART_pushString("Status code for TWISTART: ");
                Serial.println(TWSR);
            #endif

            return ERROR;
        }
        
    return TWSR;
}

uint8_t TWIWrite(uint8_t reg){

    TWDR = reg;
    TWCR = (1<<TWINT)|(1<<TWEN); 
    while (!(TWCR & (1<<TWINT)));

    if ((TWSR == BUS_ERROR) || 
        (TWSR == ARBILOST) ||  
        (TWSR == SLAW_T_NACK) ||  
        (TWSR == SLAR_T_NACK) ||
        (TWSR == DATA_T_NACK)) {

            #ifdef DEBUG 
                UART_pushString("Status code for TWIWrite: ");
                Serial.println(TWSR);
            #endif

            return ERROR;
        }
        
    return TWSR;
}

uint8_t TWIRead_ACK() {

    TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
    while (!(TWCR & (1<<TWINT)));

    if ((TWSR == BUS_ERROR) || 
        (TWSR == ARBILOST) ||
        (TWSR == DATA_R_NACK)) {

            #ifdef DEBUG 
                UART_pushString("Status code for TWIRead: ");
                Serial.println(TWSR);
            #endif

            return ERROR;
        }
        
    return TWDR;
}

uint8_t TWIRead_NACK() {

    TWCR = (1<<TWINT)|(1<<TWEN);
    while (!(TWCR & (1<<TWINT)));

    if ((TWSR == BUS_ERROR) || 
        (TWSR == ARBILOST)) {

            #ifdef DEBUG 
                UART_pushString("Status code for TWIRead: ");
                Serial.println(TWSR);
            #endif

            return ERROR;
        }
        
    return TWDR;
}

void TWIStop(){
    TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
}